// Rashed Kamal 2034147
package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bikes = new Bicycle[4]; //initialize array of bikes
        bikes[0] = new Bicycle("Specialized", 10, 100);
        bikes[1] = new Bicycle("Bixi", 3, 80);
        bikes[2] = new Bicycle("Framework", 6, 150);
        bikes[3] = new Bicycle("Yamaha", 15, 300);
        
        for(int i=0; i < bikes.length; i++){ //cycle through bikes[]
            System.out.println(bikes[i]);
        }
    }
}